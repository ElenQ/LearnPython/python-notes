# Python 3

This repository are the notes for the students of a Python3 course Ekaitz gave
the 8th of March of 2018.

Check the [PythonIntro.pdf](PythonIntro.pdf) file for the contents.

# License

They are released under CCBY 3.0 license terms.

All the functional parts of the repository are copied from:

https://gitlab.com/ElenQ/documentation-templates

And released with Apache2.0 license
