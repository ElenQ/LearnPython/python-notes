# Intro

> Python is an interpreted high-level programming language for general-purpose
> programming. Created by Guido van Rossum and first released in 1991, Python
> has a design philosophy that emphasizes code readability, and a syntax that
> allows programmers to express concepts in fewer lines of code, notably using
> significant whitespace. It provides constructs that enable clear programming
> on both small and large scales.
>
> Python features a dynamic type system and automatic memory management. It
> supports multiple programming paradigms, including object-oriented,
> imperative, functional and procedural, and has a large and comprehensive
> standard library.
>
> — Wikipedia

## REPL (Read, Eval, Print and Loop)

The REPL reads the code you type, evaluates it, prints the result and reads
again and... That's why it's called REPL.

All the code pieces in this guide are executed in the python REPL. To run it
just run `python` in your terminal or use you code editor's console.

## Philosophy

```

>>> import this
The Zen of Python, by Tim Peters

Beautiful is better than ugly.
Explicit is better than implicit.
Simple is better than complex.
Complex is better than complicated.
Flat is better than nested.
Sparse is better than dense.
Readability counts.
Special cases aren't special enough to break the rules.
Although practicality beats purity.
Errors should never pass silently.
Unless explicitly silenced.
In the face of ambiguity, refuse the temptation to guess.
There should be one-- and preferably only one --obvious way to do it.
Although that way may not be obvious at first unless you're Dutch.
Now is better than never.
Although never is often better than *right* now.
If the implementation is hard to explain, it's a bad idea.
If the implementation is easy to explain, it may be a good idea.
Namespaces are one honking great idea -- let's do more of those!

```

# General syntax

Python's syntax is really simple.

- Next line defines the end of the command.
- Code blocks are defined with indentation. Take it seriously!

## Comments

Anything after a `#` will be considered a comment and will be ignored by the
interpreter.



# Variables and Types

`=` operator binds a name to a value.

## Basic types

All basic types are **immutable**.

### Integer

Positive or negative.

``` python
>>> a = 10
>>> a
10
```

### Float

Positive or negative.

``` python
>>> a = 1.23324
>>> a
1.23324
```

### String

Delimiter can be `"` or `'`.

``` python
>>> a = "String"
>>> a
'String'
```
_\*Characters are strings with length 1_

### Boolean

Only `True` or `False`.

``` python
>>> a = True
>>> a
True
```

## Complex Types

### Tuples

Tuples are a collection of values.

Tuples are **immutable**.

``` python
>>> a = ('1', 23, -12.3232)
>>> a
('1', 23, -12.3232)
>>> a[0]
'1'
>>> a[1]
23
>>> a[2]
-12.3232
>>> a[-1]
-12.3232
```

### Lists

Like a tuple but **mutable**.

Lists have worse performance than Tuples, but much more functionality.

``` python
>>> a = ['1', 23, -12.3232]
>>> a
['1', 23, -12.3232]
>>> a.append("extra")
>>> a
['1', 23, -12.3232, 'extra']
```

### Dictionaries

Like Lists or Tuples but with named keys.

Dictionaries are **mutable**.

Key-value pairs are not stored in order.

``` python
 a = {"first": '1', "second": 23, "third":-12.3232}
>>> a
{'second': 23, 'first': '1', 'third': -12.3232}
>>> a["third"]
-12.3232
>>> a["fourth"] = 2
>>> a
{'second': 23, 'first': '1', 'third': -12.3232, 'fourth': 2}
```

## Everything is a reference

``` python
>>> a = []
>>> b = a
>>> b.append(1)
>>> a
[1]
>>> b
[1]
```

No problem with immutable types because they can't be changed. Be careful with
mutability!

``` python
>>> a = []
>>> a = a.copy() # Make a shallow copy
>>> b.append(1)
>>> a
[1]
>>> b
[]
```

# Flow control

## Conditionals

Conditionals separate the execution in branches depending on a condition.

### if

``` python
if condition1:
    # body for condition1
    # This is only processed if condition1 is evaluated to True
elif condition2:
    # body for condition2
...
elif conditionN:
    # body for conditionN
else:
    # body for others
```

`elif` and `else` blocks are optional.

Conditions must evaluate to `True` or `False`.

Specific values that are evaluated to false:
[link](https://docs.python.org/3/library/stdtypes.html#truth-value-testing)


## Loops

Loops repeat a piece of code depending on a condition.

### while

Repeats until the condition is not matched.

``` python
while a < 10:
    # will be repeated until `a` is greater or equal than 10
```


### for

**for** iterates through the elements on a sequence \*.

``` python
for i in [1,2,3,4]:
    # will be repeated four times changing `i`'s value with the
    # contents of the array
    if i == 4:
        break
else:
    # This block happens when the previous block doesn't end with a `break`
    # Else is optional.
    # For this example it's not executed. *See next section*.
```

_\*Lists, Tuples, Strings and Dictionaries behave like sequences. Later in OOP
chapter is explained why._

### break-ing, continue-ing and pass-ing

Some statements can affect to loops:

- `break`: breaks the current loop and the execution continues after it.
- `continue`: jumps to the next iteration discarding the rest of the
  expressions in the loop.
- `pass`: does nothing, but in python is mandatory to fill the indented blocks.
  Is useful for infinite loops or empty functions.

# Operators

``` python
>>> 1 + 2
3
>>> 1 == 2
False
```

## Truth value testing

There are many operators for truth value testing. Most of them are useful in
loops and conditionals. They return `True` or `False`.
[link](https://docs.python.org/3/library/stdtypes.html#truth-value-testing)

| operator | meaning |
|---|---|
| <  | strictly less than |
| <=  | less than or equal |
| >  | strictly greater than |
| >=  | greater than or equal |
| ==  | equal |
| !=  | not equal |
| is  | object identity |
| is not  | negated object identity |
| in  | containment test |
| not in  | negated containment test |

## Mathematical

| operator | meaning |
|---|---|
| + | addition |
| - | subtraction or negative |
| `*` | multiplication |
| / | division |
| `**` | power |
| % | remainder |

# Functions

Functions are pieces of code gathered together to simplify the job and
encourage code reuse.

``` python
def fib(num):
    """
    This is a documentation block called `docstring`. It's optional.
    Read it with `help(fib)`.
    """
    res = []
    a, b = 0, 1
    while a < num:
        res.append(a)
        a, b = b, a+b
    return res              # exit with `res` value

>>> fib(10)
[0, 1, 1, 2, 3, 5, 8]
>>> result = fib(40)
>>> result
[0, 1, 1, 2, 3, 5, 8, 13, 21, 34]
```

Functions can be moved around like variables, too.

``` python
>>> fib
<function fib at 0x7fd008eb2578>
```

_\* [More info](https://docs.python.org/3.6/tutorial/controlflow.html#more-on-defining-functions)_

## Memory and Scope

Python has a *garbage-collector* this means it automatically controls the
memory, cleaning all the variables which are not in use.

The *scope* is the piece of code where a variable is accessible.

The simplest rule of the scope is:

> Scope goes from the current indentation level to deeper indentation levels.


Consider this:

``` python
>>> def test1():
...     a = 1     # Local variable declaration, only accessible here
...
>>> def test2():
...     return a  # Not declared, a is out of scope
...
>>> test2()
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
  File "<stdin>", line 2, in test2
NameError: global name 'a' is not defined
```

But:

``` python
>>> a = 10
>>> def test2():
...     return a  # `a` is declared in the upper level, it has access
...
>>> test2()
10
```

### Closures

Functions save a copy of their environment when they are declared.

``` python
def func_creator():
    a = 10                  # local variable
    def incrementer( arg ):
        return arg + a      # `a` is in scope
    return incrementer

>>> inc = func_creator()
>>> inc
<function incrementer at 0x7f52f8947668>
>>> inc(10)                    # incrementer function remembers `a`
20
>>> a                          # Not in scope!
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
NameError: name 'a' is not defined
```

### lambda

Lambdas are unnamed functions.

``` python
>>> inc = lambda x: x+1
>>> inc(1)
2
>>> inc(10)
11
>>> inc
<function <lambda> at 0x7f52f8947758>
```

# Files

``` python
>>> f = open('src/contenido.md', 'r')     # `r` is the mode
>>> f.read()[0:100]
'# Python 3\n\n## Intro\n\n> Python is an interpreted high-level ...'
>>> f.close()
```

More elegantly:

``` python
>>> with open('src/contenido.md', 'r') as f:
...     f.read()[0:100]
...
'# Python 3\n\n## Intro\n\n> Python is an interpreted high-level ...'
```

# Modules and packages

A Python module is simply a Python source file, which can expose classes,
functions and global variables.

A Python package is a directory of Python module(s).

In directories, `__init__.py` file is used to tell python that the directory is
a package.

## Load a module

``` python
>>> import math      # imports math and leaves it in `math` namespace
>>> math.pow(2, 4)   # `math` prefix is needed
16.0
>>> from math import pow     # from `math` only import pow to the local scope
>>> pow(2, 4)                # no prefix needed
16.0
>>> from math import pow as p  # like before but with a different name
>>> p(2,4)
16.0
```

Packages can contain subfolders. They are treated as namespaces in the import:
`import package.module` or `import package.subpackage.module`.

### Search path

The variable `sys.path` contains the paths where the modules are searched.

Default is:

  - Executed file's directory or the current directory in the REPL.
  - PYTHONPATH: an environment variable with directories.
  - The installation-dependent default.

## Execute a module

Run `python modulename.py [args]` in the command line. This runs the module
with `__name__` set to `__main__`

``` python
if __name__ == "__main__":
    import sys                # This block won't run if the module is imported
    fib(int(sys.argv[1]))     # but it will when it's executed directly
```

## Tools

Python has different tools for dependency management and package installation:
`pip`, `easy_install`...

Also some virtual environment tools to separate development contexts like:
`virtualenv-wrapper`, `pipenv`...


# OOP (Object Oriented Programming)

``` python
class Dog:

    kind = 'canine'         # class variable shared by all instances

    def __init__(self, name):
        self.name = name    # instance variable unique to each instance
    def bark_name(self):    # this is a class method
        print('Wooof' + self.name) # `self` is used to access the instance

>>> d = Dog('Fido')         # create a new instance named Fido
>>> e = Dog('Buddy')        # create a new instance named Buddy
>>> d.kind                  # shared by all dogs
'canine'
>>> e.kind                  # shared by all dogs
'canine'
>>> d.name                  # unique to d
'Fido'
>>> e.name                  # unique to e
'Buddy'
>>> e.bark_name()
'Wooof Buddy'
```

## Inheritance

Classes can be inherited: `class Dog(Animal)`

## Interfaces

An interface is just a group of method a class must have to be able to interact
in a standard way.

For example, the iterator interface defines which methods are needed to act as
an iterator.

This is an iterator class:

``` python
class Counter:
    def __init__(self, low, high):
        self.current = low
        self.high = high

    def __iter__(self):
        return self

    def __next__(self):
        if self.current > self.high:
            raise StopIteration
        else:
            self.current += 1
            return self.current - 1

>>> for c in Counter(3, 8): # Prints 3,4,5,6,7,8
>>>    print c
```

# More info

- [The python tutorial](https://docs.python.org/3.6/tutorial/index.html)
- [The hitchhiker's guide to python!](http://docs.python-guide.org/en/latest/)
- [Python tips (advanced)](http://book.pythontips.com/en/latest/index.html)
